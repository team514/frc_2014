/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

import edu.mphs.first.wpilibj.RobotMap;

/**
 *
 * @author marnold
 */
public class TrackTargets extends CommandBase {
    boolean side;
    int timestamp;
    public TrackTargets(boolean side) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(visionUtil);
        requires(timerUtil);
        this.side = side;
        timestamp = 0;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
        timerUtil.calcTime();
        timestamp = timerUtil.getSeconds();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        timerUtil.calcTime();
        visionUtil.setSide(side);
        if(timerUtil.getSeconds() >= RobotMap.start_Vision){
            if(takeALook()){
                visionUtil.processImages();            
            }
        }
   }
    
    private boolean takeALook(){
        boolean look = false;
        if(timerUtil.getSeconds() > timestamp){
            timestamp = timerUtil.getSeconds();
            look = true;
        }
        return look;
    }
    

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        boolean done;
        done = false;
        if(timerUtil.getSeconds() >= RobotMap.stop_Vision){
            done = true;
        }
        if(visionUtil.getHotGoal()){
            done = true;
        }
        return done;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
