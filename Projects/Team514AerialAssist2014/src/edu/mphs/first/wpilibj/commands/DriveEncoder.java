/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

import edu.mphs.first.wpilibj.RobotMap;

/**
 *
 * @author marnold
 */
public class DriveEncoder extends CommandBase {
    double leftValue, rightValue;
    boolean test;
    public DriveEncoder(boolean test) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(driveUtil);
        requires(encoderUtil);
        requires(timerUtil);
        this.test = false;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
        encoderUtil.resetEncoder();
        encoderUtil.startEncoder();
        timerUtil.resetTimer();
        timerUtil.startTimer();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        timerUtil.calcTime();
        if(test){
            driveUtil.drive(0.0, 0.0);
        }else{
            if(encoderUtil.getLeftEncoderDistance() >= RobotMap.ENCODER_DISTANCE){
                driveUtil.drive(0.0, 0.0);
                encoderUtil.setShotRange(true);
                encoderUtil.stopEncoder();
            }else{
                encoderUtil.setShotRange(false);            
                getMotorSpeed();
                driveUtil.drive(leftValue, rightValue);
            }
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return encoderUtil.getShotRange();
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
    
    public void getMotorSpeed(){
        double delta, adjust;
        leftValue = RobotMap.auto_LeftMotor;
        rightValue = RobotMap.auto_RightMotor;
        
        delta = encoderUtil.getLeftEncoderDistance() - encoderUtil.getRightEncoderDistance();
    
        adjust = encoderUtil.coerce2Range(delta);
        
        rightValue = rightValue + adjust;
        leftValue = leftValue + -adjust;
        
    }
    
}
