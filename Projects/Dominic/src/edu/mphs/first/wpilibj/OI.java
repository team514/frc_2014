
package edu.mphs.first.wpilibj;

import edu.mphs.first.wpilibj.commands.DriveArcade;
import edu.mphs.first.wpilibj.commands.DriveTank;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
    Joystick leftstick, rightstick;
    JoystickButton tank, arcade;
    
    public OI(){
        leftstick = new Joystick(RobotMap.LEFT_STICK);
        rightstick = new Joystick(RobotMap.RIGHT_STICK);
        
        tank = new JoystickButton(rightstick, RobotMap.TANK_MODE);
        arcade = new JoystickButton(rightstick, RobotMap.ARCADE_MODE);
        
        tank.whenPressed(new DriveTank());
        arcade.whenPressed(new DriveArcade());
        
        
    }
    
    public Joystick getLeftstick(){
        return leftstick;
    }
    
    public Joystick getRightstick(){
        return rightstick;
    }
}

