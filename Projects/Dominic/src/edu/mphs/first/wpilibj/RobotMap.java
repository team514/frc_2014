package edu.mphs.first.wpilibj;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
    // For example to map the left and right motors, you could define the
    // following variables to use with your drivetrain subsystem.
    // public static final int leftMotor = 1;
    // public static final int rightMotor = 2;
    
    // If you are using multiple modules, make sure to define both the port
    // number and the module. For example you with a rangefinder:
    // public static final int rangefinderPort = 1;
    // public static final int rangefinderModule = 1;
    
    // **************  Constants and Values     (ClassName/ClassObjectName/RobotMapVariablePrefix ****** //
        // Compressor Subsystem (CompressorUtil/compUtil/cmp)
        
        // Drive Subsystem (DriveUtil/driveUtil/drv)
        
    // **************  Physical Device Mapping  ************************** //
        // Digital Side Car Mapping
            // All PWMs go here!
            public static final int drv_LEFT_MOTOR = 2;
            public static final int drv_RIGHT_MOTOR = 1;

            // All DIO go here!
            
            // All Relays go here!
            
            // All Analog Modules go here!
            
            // All PNEUMATIC Modules go here!
    
        // Right Joystick and Buttons
        public static final int RIGHT_STICK = 2;
        public static final int TANK_MODE = 2;
        public static final int ARCADE_MODE = 3;
    
        // Left Joystick and Buttons
        public static final int LEFT_STICK = 1;
    
        // Controller and Buttons
        public static final int CONTROLLER = 3;
        
        
    
    
}
