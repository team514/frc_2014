/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.subsystems;

import edu.mphs.first.wpilibj.RobotMap;
import edu.mphs.first.wpilibj.commands.DriveTank;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 * @author marnold
 */
public class DriveUtil extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    
    RobotDrive drive;
    SpeedController leftdrive, rightdrive;
    
    public DriveUtil(){
        leftdrive = new Victor(RobotMap.drv_LEFT_MOTOR);
        rightdrive = new Victor(RobotMap.drv_RIGHT_MOTOR);
        drive = new RobotDrive(leftdrive, rightdrive);
        drive.setInvertedMotor(RobotDrive.MotorType.kRearLeft, true);
        drive.setInvertedMotor(RobotDrive.MotorType.kRearRight, true);
    }
    
    public void driveTank(Joystick left, Joystick right){
        drive.tankDrive(left, right);
    }
    
    public void driveArcade(Joystick stick){
        drive.arcadeDrive(stick);
        
    }

    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        setDefaultCommand(new DriveTank());
        
    }
}
