/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

import edu.mphs.first.wpilibj.commands.CommandBase;

/**
 *
 * @author marnold
 */
public class Drive5Feet extends CommandBase {
    boolean done;
    
    public Drive5Feet() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(driveUtil);
        requires(sonarUtil);
        
        done = false;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        driveUtil.drive(0.57, 0.45);
        if(sonarUtil.getRangeInches() <= 120.0){
            driveUtil.drive(0.0, 0.0);
            done = true;
            
        }else{
            done = false;
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return done;
        //return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
