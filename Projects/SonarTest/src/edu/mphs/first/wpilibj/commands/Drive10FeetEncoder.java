/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

import edu.mphs.first.wpilibj.RobotMap;
import edu.mphs.first.wpilibj.commands.CommandBase;

/**
 *
 * @author Team 514
 */
public class Drive10FeetEncoder extends CommandBase {
    boolean done;
    
    
    public Drive10FeetEncoder() {
        // Use requires() here to declare subsystem dependencies
        requires(driveUtil);
        requires(encoderUtil);
        
        done = false;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
       
        
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
      
        
        driveUtil.drive(0.57, 0.45);
        if(encoderUtil.getEncoderDistance() >= RobotMap.eDistance_10ft){
            driveUtil.drive(0.0, 0.0);
            done = true;
            
        }else{
            done = false;
    }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
