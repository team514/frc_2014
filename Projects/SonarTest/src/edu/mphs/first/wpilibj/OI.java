
package edu.mphs.first.wpilibj;

import edu.mphs.first.wpilibj.commands.Drive10FeetEncoder;
import edu.mphs.first.wpilibj.commands.Drive5Feet;
import edu.mphs.first.wpilibj.commands.DriveArcade;
import edu.mphs.first.wpilibj.commands.DriveTank;
import edu.mphs.first.wpilibj.commands.ResetEncoder;

//import edu.mphs.first.wpilibj.commands.Resetsonar;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
    Joystick leftstick, rightstick, controller;
    JoystickButton tank, arcade, reset; 
    JoystickButton Drive5Feet, Drive10Feet, EncoderReset;
    
    public OI(){
        leftstick = new Joystick(RobotMap.LEFT_STICK);
        rightstick = new Joystick(RobotMap.RIGHT_STICK);
        controller = new Joystick(RobotMap.CONTROLLER);
        
        tank = new JoystickButton(rightstick, RobotMap.TANK_MODE);
        arcade = new JoystickButton(rightstick, RobotMap.ARCADE_MODE);
        reset = new JoystickButton(controller, 1);
        Drive5Feet = new JoystickButton(controller, 3);
        Drive10Feet = new JoystickButton(controller, 4);
        EncoderReset = new JoystickButton(controller, 2);
        
        
        
        tank.whenPressed(new DriveTank());
        arcade.whenPressed(new DriveArcade());
        //reset.whenPressed(new Resetsonar());
        Drive5Feet.whenPressed(new Drive5Feet());
        Drive10Feet.whenPressed(new Drive10FeetEncoder());
        EncoderReset.whileHeld(new ResetEncoder());
        
        
        //AutonomousMode Selections
        //SmartDashboard.putData("Left Side", new AutoCMD(1));
        //SmartDashboard.putData("Right Side", new AutoCMD(2));
        
    }
    
    public Joystick getLeftstick(){
        return leftstick;
    }
    
    public Joystick getRightstick(){
        return rightstick;
    }
}

