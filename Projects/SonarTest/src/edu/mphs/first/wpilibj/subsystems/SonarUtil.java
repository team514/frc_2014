/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.subsystems;

import edu.mphs.first.wpilibj.RobotMap;
import edu.wpi.first.wpilibj.AnalogChannel;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DigitalOutput;
import edu.wpi.first.wpilibj.Ultrasonic;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 * @author marnold
 */
public class SonarUtil extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    
    //AnalogChannel sonar;
    DigitalInput echo;
    DigitalOutput ping;
    Ultrasonic sonar;
    
    public SonarUtil(){
        //sonar = new AnalogChannel(RobotMap.SONAR);
        //sonar.setAccumulatorInitialValue(0);
        echo = new DigitalInput(8);
        ping = new DigitalOutput(7);
        sonar = new Ultrasonic(ping, echo, Ultrasonic.Unit.kInches);
        sonar.setAutomaticMode(false);
        sonar.setEnabled(true);
    }
    
    private double calcRange(){
        double range;
        //range = (sonar.getAverageVoltage()/RobotMap.D2V_Constant);
        sonar.ping();
        if(sonar.isRangeValid()){
            range = sonar.getRangeInches();
        }else{
            range = 0.0;
            sonar.ping();
        }
        //range = sonar.getRangeInches();
        return range;
    }
    
    public double getRangeInches(){
        double range;
        range = calcRange();
        return range;
    }
    
    public double getRangeFeet(){
        double range;
        range = (calcRange()/12);
        return range;
    }
    
    public void updateStatus(){
        //SmartDashboard.putNumber("Sonar Range InchesVoltage = ", sonar.getRangeInches());
        SmartDashboard.putNumber("Range Inches = ", getRangeInches());
        SmartDashboard.putNumber("Range Feet = ", getRangeFeet());
    }

    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}
