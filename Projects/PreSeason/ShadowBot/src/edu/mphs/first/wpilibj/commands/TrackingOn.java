/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

/**
 *
 * @author Team 514
 */
public class TrackingOn extends CommandBase {
    int limit;
    boolean lastDirection;
    
    public TrackingOn() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(targetsUtil);
        requires(angleUtil);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
        targetsUtil.setTarget(3);
        angleUtil.disableMotor();
        limit = 0;
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        
        targetsUtil.manageTargets();
        if(targetsUtil.foundTarget()){
            followY(targetsUtil.getDistance(), targetsUtil.getYDirection(), targetsUtil.onTarget());  
        }else{
            if(limit < 3){
                if(!angleUtil.getLower()  &&  
                   !angleUtil.getUpper()){
                    if(lastDirection){
                        angleUtil.enableForward();
                        lastDirection = true;
                    }else{
                        angleUtil.enableReverse();
                        lastDirection = false;
                    }
                }else{
                     if(angleUtil.getLower()){
                    angleUtil.enableForward();
                    lastDirection = true;
                    limit++;
                }
                if(angleUtil.getUpper()){
                    angleUtil.enableReverse();
                    lastDirection = false;
                    limit++;
                 }

                }
            }
        }
    }
    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        boolean done;
        done = false;
        if(limit >= 3){
            done = true;
        }
        return done;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
    private void followY(double distance, boolean direction, boolean onTarget){
        if(onTarget){
            angleUtil.disableMotor();
        }else{
            if(direction){
                angleUtil.enableForward();
                lastDirection = direction;
            }else{
                angleUtil.enableReverse();
                lastDirection = direction;
            }
        }
    }
}
