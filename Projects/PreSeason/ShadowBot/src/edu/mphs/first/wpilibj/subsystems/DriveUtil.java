/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.subsystems;

import edu.mphs.first.wpilibj.RobotMap;
import edu.mphs.first.wpilibj.commands.DriveInTank;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 * @author Team 514
 */
public class DriveUtil extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    RobotDrive robotDrive;
    SpeedController leftMotor, rightMotor;
    int driveMode;
    double left, right;
    
    public DriveUtil(){
        leftMotor = new Victor(RobotMap.drv_LEFT_MOTOR);
        rightMotor = new Victor(RobotMap.drv_RIGHT_MOTOR);
        robotDrive = new RobotDrive(leftMotor, rightMotor);
        robotDrive.setInvertedMotor(RobotDrive.MotorType.kRearLeft, true);
        robotDrive.setInvertedMotor(RobotDrive.MotorType.kRearRight, true);
    }
    
    public void drive(double left, double right){
        robotDrive.tankDrive(left, right);
    }
    
    public void driveTank(Joystick left, Joystick right){
        robotDrive.tankDrive(left, right);
        setDriveMode(RobotMap.TANK_MODE);
        setDriveSpeed(left, right);
    }
    
    public void driveArcade(Joystick right){
        robotDrive.arcadeDrive(right);
        setDriveMode(RobotMap.ARCADE_MODE);
        setDriveSpeed(right);
    }

    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        setDefaultCommand(new DriveInTank());
    }
    
    public String getDriveMode(){
        String mode;
        switch(this.driveMode){
            case(RobotMap.TANK_MODE):
                mode = "TANK";
                break;
            case(RobotMap.ARCADE_MODE):
                mode = "ARCADE";
                break;
            default:
                mode = "Not Set";
                break;
    }
        
        return mode;
        
    }
    
    private void setDriveMode(int mode){
        this.driveMode = mode;
    }
    
    private void setDriveSpeed(Joystick left, Joystick right){
        this.left = left.getY();
        this.right = right.getY();
    }
    
    private void setDriveSpeed(Joystick right){
        this.right = right.getY();
        this.left = right.getX();
    }
    
    public double getRightSpeed(){
        return this.right;
    }
    
    public double getLeftSpeed(){
        return this.left;
    }
    
    public void updateStatus(){
        SmartDashboard.putString("Drive Mode : ", getDriveMode());
        SmartDashboard.putNumber("Right Motor : ", -getRightSpeed());
        SmartDashboard.putNumber("Left Motor : ", -getLeftSpeed());
    }
}
