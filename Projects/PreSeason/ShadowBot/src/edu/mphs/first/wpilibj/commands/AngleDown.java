/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

/**
 *
 * @author Team 514
 */
public class AngleDown extends CommandBase {
    
    public AngleDown() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(angleUtil);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        if(angleUtil.getLower()){
            angleUtil.disableMotor();
        } else {
            angleUtil.enableReverse();
        }

    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return angleUtil.getLower();
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
