
package edu.mphs.first.wpilibj;

import edu.mphs.first.wpilibj.commands.AngleDown;
import edu.mphs.first.wpilibj.commands.AngleUp;
import edu.mphs.first.wpilibj.commands.DriveInArcade;
import edu.mphs.first.wpilibj.commands.DriveInTank;
import edu.mphs.first.wpilibj.commands.TrackingOff;
import edu.mphs.first.wpilibj.commands.TrackingOn;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.DigitalIOButton;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
    //// CREATING BUTTONS
    // One type of button is a joystick button which is any button on a joystick.
    // You create one by telling it which joystick it's on and which button
    // number it is.
    // Joystick stick = new Joystick(port);
    // Button button = new JoystickButton(stick, buttonNumber);
    
    // Another type of button you can create is a DigitalIOButton, which is
    // a button or switch hooked up to the cypress module. These are useful if
    // you want to build a customized operator interface.
    // Button button = new DigitalIOButton(1);
    
    // There are a few additional built in buttons you can use. Additionally,
    // by subclassing Button you can create custom triggers and bind those to
    // commands the same as any other Button.
    
    //// TRIGGERING COMMANDS WITH BUTTONS
    // Once you have a button, it's trivial to bind it to a button in one of
    // three ways:
    
    // Start the command when the button is pressed and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whenPressed(new ExampleCommand());
    
    // Run the command while the button is being held down and interrupt it once
    // the button is released.
    // button.whileHeld(new ExampleCommand());
    
    // Start the command when the button is released  and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whenReleased(new ExampleCommand());
    Joystick leftstick, rightstick, controller;
    JoystickButton Tank, Arcade, TrackOn, TrackOff, AngleUp, AngleDown;
    

    public OI(){
        leftstick = new Joystick(RobotMap.LEFT_STICK);
        rightstick = new Joystick(RobotMap.RIGHT_STICK);
        Tank = new JoystickButton(rightstick, RobotMap.TANK_MODE);
        Arcade = new JoystickButton(rightstick, RobotMap.ARCADE_MODE);
        controller = new Joystick(RobotMap.CONTROLLER);
        TrackOn = new JoystickButton(controller, 2);
        TrackOff = new JoystickButton(controller, 3);
        AngleUp = new JoystickButton(controller, 5);
        AngleDown = new JoystickButton(controller, 7);
        
        
        
        Tank.whenPressed(new DriveInTank());
        Arcade.whenPressed(new DriveInArcade());
        TrackOn.whenPressed(new TrackingOn());
        TrackOff.whenPressed(new TrackingOff());
        AngleUp.whileHeld(new AngleUp());
        AngleDown.whileHeld(new AngleDown());
        
    }
    
    public Joystick getLeftStick(){
        return leftstick;
    }
    
    public Joystick getRightStick(){
        return rightstick;
    }
    
}


