/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

/**
 *
 * @author Team 514
 */
public class AngleUp extends CommandBase {
    
    public AngleUp() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(angleUtil);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
   
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        if(angleUtil.getUpper()){
            angleUtil.disableMotor();
        } else{
            angleUtil.enableForward();
        }
    
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return angleUtil.getUpper();
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
