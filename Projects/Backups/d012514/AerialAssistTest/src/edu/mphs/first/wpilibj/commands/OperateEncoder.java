/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

import edu.mphs.first.wpilibj.RobotMap;

/**
 *
 * @author marnold
 */
public class OperateEncoder extends CommandBase {
    int cmd;
    
    public OperateEncoder(int cmd) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(encoderUtil);
        this.cmd = cmd;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        switch (cmd){
            case RobotMap.reset_Encoder:
                encoderUtil.resetEncoder();
                break;
            case RobotMap.stop_Encoder:
                encoderUtil.stopEncoder();
                break;
            case RobotMap.start_Encoder:
                encoderUtil.startEncoder();
                break;
            default:
                encoderUtil.stopEncoder();
                break;
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
