/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.subsystems;

import edu.wpi.first.wpilibj.CounterBase;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 * @author marnold
 */
public class EncoderUtil extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    Encoder encoder;
    
    public EncoderUtil(){
        encoder = new Encoder(1, 2, false, CounterBase.EncodingType.k4X);
        encoder.stop();
        encoder.reset();
        encoder.start();

    }
    
    public void stopEncoder(){
        encoder.stop();
    }
    
    public void startEncoder(){
        encoder.start();
    }
    
    public void resetEncoder(){
        encoder.reset();
    }
    
        public double getEncoderDistance(){
        return encoder.getDistance();
    }
    
    public void updateStatus(){
        SmartDashboard.putNumber("Encoder Value = ", encoder.getDistance());
    }

    
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}
