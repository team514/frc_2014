/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

/**
 *
 * @author marnold
 */
public class DriveTeleOp extends CommandBase {
    int mode;
    
    public DriveTeleOp(int mode) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(driveUtil);
        this.mode = mode;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        switch (mode){
            case(1):
                driveUtil.driveArcade(oi.getRightstick());
                break;
            case(2):
                driveUtil.driveTank(oi.getLeftstick(), oi.getRightstick());
                break;
            default:
                driveUtil.driveTank(oi.getLeftstick(), oi.getRightstick());
                break;                
        }
            
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
