
package edu.mphs.first.wpilibj;

import edu.mphs.first.wpilibj.commands.DriveEncoder;
import edu.mphs.first.wpilibj.commands.DriveTeleOp;
import edu.mphs.first.wpilibj.commands.OperateEncoder;
import edu.mphs.first.wpilibj.commands.TrackTargets;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
    Joystick leftstick, rightstick, controller;
    JoystickButton tank, arcade, encoderDrv, e_Reset, trackOn, trackOff;
    
    public OI(){
        leftstick = new Joystick(RobotMap.LEFT_STICK);
        rightstick = new Joystick(RobotMap.RIGHT_STICK);
        controller = new Joystick(RobotMap.CONTROLLER);
        
        tank = new JoystickButton(rightstick, RobotMap.TANK_Button);
        arcade = new JoystickButton(rightstick, RobotMap.ARCADE_Button);
        encoderDrv = new JoystickButton(controller, RobotMap.encoder_Test);
        e_Reset = new JoystickButton(controller, RobotMap.encoder_Reset);
        trackOn = new JoystickButton(controller, RobotMap.vision_On);
        trackOff = new JoystickButton(controller, RobotMap.vision_Off);
        
        tank.whenPressed(new DriveTeleOp(RobotMap.tank_Mode));
        arcade.whenPressed(new DriveTeleOp(RobotMap.arcade_Mode));
        encoderDrv.whenPressed(new DriveEncoder());
        e_Reset.whenPressed(new OperateEncoder(RobotMap.reset_Encoder));
        trackOn.whenPressed(new TrackTargets(true));
        trackOff.whenPressed(new TrackTargets(false));
        
        //AutonomousMode Selections
        //SmartDashboard.putData("Left Side", new AutoCMD(1));
        //SmartDashboard.putData("Right Side", new AutoCMD(2));
        
    }
    
    public Joystick getLeftstick(){
        return leftstick;
    }
    
    public Joystick getRightstick(){
        return rightstick;
    }
}

