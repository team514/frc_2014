package edu.mphs.first.wpilibj;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
    // For example to map the left and right motors, you could define the
    // following variables to use with your drivetrain subsystem.
    // public static final int leftMotor = 1;
    // public static final int rightMotor = 2;
    
    // If you are using multiple modules, make sure to define both the port
    // number and the module. For example you with a rangefinder:
    // public static final int rangefinderPort = 1;
    // public static final int rangefinderModule = 1;
    
    // **************  Constants and Values     (ClassName/ClassObjectName/RobotMapVariablePrefix ****** //
        // Compressor Subsystem (CompressorUtil/compUtil/cmp)
        
        // Drive Subsystem (DriveUtil/driveUtil/drv)
        // Encoder Data = 4" Wheel Diameter, 12.57" Circumference
        // 174 Ticks/Inch
            public static final double ENCODER_DISTANCE = 1661.10;
        
    // **************  Physical Device Mapping  ************************** //
        // Digital Side Car Mapping
            // All PWMs go here!
            public static final int drv_LEFT_MOTOR = 2;
            public static final int drv_RIGHT_MOTOR = 1;
            public static final int SONAR = 2;

            // All DIO go here!
            public static final int uSonic_echo_Input = 1;
            public static final int uSonic_ping_Output = 2;
            
            // All Relays go here!
            
            // All Analog Modules go here!
            
            // All PNEUMATIC Modules go here!
    
        // Right Joystick and Buttons
        public static final int RIGHT_STICK = 2;
        public static final int TANK_Button = 2;
        public static final int ARCADE_Button = 3;
    
        // Left Joystick and Buttons
        public static final int LEFT_STICK = 1;
    
        // Controller and Buttons
        public static final int CONTROLLER = 3;
        public static final int PING = 1;
        public static final int encoder_Reset = 2;
        public static final int encoder_Test = 4;
        public static final int vision_On = 5;
        public static final int vision_Off = 7;
        
        
        
        //Declare Sonar Constants
        public static final double D2V_Constant = 0.009766;
        
        //Declare Encoder Constants
        public static final int stop_Encoder = 2;
        public static final int start_Encoder = 3;
        public static final int reset_Encoder = 1;
        
        //Declare Drive Mode Constants
        public static final int tank_Mode = 2;
        public static final int arcade_Mode = 1;
        public static final double auto_LeftMotor = 0.57;
        public static final double auto_RightMotor = 0.45;
        
    
    
}
