/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

import edu.mphs.first.wpilibj.RobotMap;

/**
 *
 * @author Team 514
 */
public class OperateTension extends CommandBase {
    int mode;
    
    public OperateTension(int mode) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(shotUtil);
        requires(tensionUtil);
        this.mode = mode;
     
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
                switch(mode){
            case RobotMap.pass_ready_mode:
                if(shotUtil.GetShooterStatus()){
                    tensionUtil.releaseTension();
                }else{
                    returnShooter();
                }
            break;
            case RobotMap.fire_ready_mode:
                if(shotUtil.GetShooterStatus()){
                 tensionUtil.setTension();
                }else{
                    returnShooter();
                }
            break; 
        }

    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return tensionUtil.getTensionStatus();
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
    
    private void returnShooter(){
        tensionUtil.releaseTension();
        if(tensionUtil.isLoose()){
           shotUtil.ReturnShooter();
        }
    }
}
