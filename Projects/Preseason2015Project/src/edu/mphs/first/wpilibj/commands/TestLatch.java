/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

/**
 *
 * @author Team 514
 */
public class TestLatch extends CommandBase {
    boolean active;
    public TestLatch(boolean active) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(shotUtil);
        this.active = active;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        if(active){
            shotUtil.Fire();
        }else{
            shotUtil.ReturnLatch();
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
