/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

import edu.mphs.first.wpilibj.RobotMap;
import edu.wpi.first.wpilibj.Timer;

/**
 *
 * @author Team 514
 */
public class OperateShot extends CommandBase {
    boolean done;
    public OperateShot() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(shotUtil);
        requires(tensionUtil);
        requires(gatorUtil);
        requires(jawUtil);
        done = false;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        if(GetShotStatus()){
            shotUtil.Fire();
            Timer.delay(RobotMap.latch_WAIT);  //Could be longer if pneumatics have to charge!
            shotUtil.ReturnLatch();
            Timer.delay(RobotMap.return_WAIT);
            done = true;
        }
    }
    
    private boolean GetShotStatus(){
        boolean Status = true;
        //Status &= shotUtil.GetBallStatus();
        Status &= shotUtil.getPrimerStatus();
        Status &= shotUtil.GetShooterStatus();
        Status &= tensionUtil.getTensionStatus();
        Status &= gatorUtil.getGatorStatus();
        Status &= jawUtil.getJawStatus();
        
        return Status;
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return done;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
