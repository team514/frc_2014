/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

import edu.mphs.first.wpilibj.RobotMap;

/**
 *
 * @author Team 514
 */
public class DriveAuto extends CommandBase {
    boolean done; 
    public DriveAuto() {
        requires(driveUtil);
        requires(encoderUtil);    
       // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
        encoderUtil.startEncoder();
        encoderUtil.resetEncoder();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        double input;
        double output;
        double leftInput;
        double rightInput;
        input = encoderUtil.getRightEncoderDistance() - encoderUtil.getLeftEncoderDistance();
        output = encoderUtil.coerce2Range(input);
        if(encoderUtil.getRightEncoderDistance()>= RobotMap.ENCODER_DISTANCE){
        driveUtil.drive(0, 0);
        done = true;
    }else{
        leftInput = RobotMap.auto_LeftMotor + output;
        rightInput = RobotMap.auto_RightMotor + -output;
        driveUtil.drive(leftInput, rightInput);    
        done = false;
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
       
        return done;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
