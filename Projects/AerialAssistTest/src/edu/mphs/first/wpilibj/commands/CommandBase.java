package edu.mphs.first.wpilibj.commands;

import edu.mphs.first.wpilibj.OI;
import edu.mphs.first.wpilibj.subsystems.BallMagUtil;
import edu.mphs.first.wpilibj.subsystems.CompressorUtil;
import edu.mphs.first.wpilibj.subsystems.DriveUtil;
import edu.mphs.first.wpilibj.subsystems.EncoderUtil;
import edu.mphs.first.wpilibj.subsystems.GatorUtil;
import edu.mphs.first.wpilibj.subsystems.JawUtil;
import edu.mphs.first.wpilibj.subsystems.ShotUtil;
import edu.mphs.first.wpilibj.subsystems.TensionUtil;
import edu.mphs.first.wpilibj.subsystems.TimerUtil;
import edu.mphs.first.wpilibj.subsystems.VisionUtil;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * The base for all commands. All atomic commands should subclass CommandBase.
 * CommandBase stores creates and stores each control system. To access a
 * subsystem elsewhere in your code in your code use CommandBase.exampleSubsystem
 * @author Author
 */
public abstract class CommandBase extends Command {

    public static OI oi;
    // Create a single static instance of all of your subsystems
    public static DriveUtil driveUtil = new DriveUtil();
    public static EncoderUtil encoderUtil = new EncoderUtil();
    public static VisionUtil visionUtil = new VisionUtil();
    public static CompressorUtil compUtil = new CompressorUtil();
    public static BallMagUtil ballmagUtil = new BallMagUtil(); 
    public static GatorUtil gatorUtil = new GatorUtil();
    public static JawUtil jawUtil = new JawUtil();
    public static ShotUtil shotUtil = new ShotUtil();
    public static TensionUtil tensionUtil = new TensionUtil();
    public static TimerUtil timerUtil = new TimerUtil();
    

    public static void init() {
        // This MUST be here. If the OI creates Commands (which it very likely
        // will), constructing it during the construction of CommandBase (from
        // which commands extend), subsystems are not guaranteed to be
        // yet. Thus, their requires() statements may grab null pointers. Bad
        // news. Don't move it.
        oi = new OI();

        // Show what command your subsystem is running on the SmartDashboard
        //SmartDashboard.putData(exampleSubsystem);
        SmartDashboard.putData(driveUtil);
        SmartDashboard.putData(encoderUtil);
        SmartDashboard.putData(visionUtil);
        SmartDashboard.putData(ballmagUtil);
        SmartDashboard.putData(gatorUtil);
        SmartDashboard.putData(jawUtil);
        SmartDashboard.putData(shotUtil);
        SmartDashboard.putData(tensionUtil);
        SmartDashboard.putData(timerUtil);
    }

    public CommandBase(String name) {
        super(name);
    }

    public CommandBase() {
        super();
    }
}
