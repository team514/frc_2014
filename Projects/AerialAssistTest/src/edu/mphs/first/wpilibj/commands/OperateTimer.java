/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mphs.first.wpilibj.commands;

import edu.mphs.first.wpilibj.RobotMap;

/**
 *
 * @author Team 514
 */
public class OperateTimer extends CommandBase {
    int mode;
    boolean done;
    public OperateTimer(int mode) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(timerUtil);
        this.mode = mode;
        done = false;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        switch (mode){
        case RobotMap.timer_Start:
            timerUtil.startTimer();
            done = true;
            break;
        case RobotMap.timer_Reset:
            timerUtil.resetTimer();
            timerUtil.startTimer();
            done = true;
            break;
        case RobotMap.timer_Stop:
            timerUtil.stopTimer();
            done = true;
            break;
        case RobotMap.timer_Run:
            timerUtil.calcTime();
            done = false;
            break;
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return done;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
